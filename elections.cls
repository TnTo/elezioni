\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{elections}
\LoadClass[a4paper]{extarticle}

\PassOptionsToClass{landscape}{extarticle}
\PassOptionsToClass{20pt}{extarticle}

\usepackage[a4paper,margin=.95cm]{geometry}
\usepackage{parskip}

\usepackage{ebgaramond}
\usepackage[T1]{fontenc}

%\newfontfamily{\emojifont}{Symbola.ttf}
%\newcommand{\emoji}[1]{{\emojifont\char"#1}}

\pagestyle{empty}

\setlength{\fboxsep}{.25in}
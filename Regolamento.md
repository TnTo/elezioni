# Elezioni

## Introduzione
**Elezioni!** nasce per simulare le vivaci dinamiche di una repubblica parlamentare, tra partiti, elezioni e faide.

**Elezioni!** prova a essere l'erede di giochi come *Die Macher* o *1960: the making of the President*.

In **Elezioni!** ogni giocatore impersona un partito politico che cerca di farsi strada fino al governo.

***Tutti i valori numerici sono provvisori***  
***Si possono pensare degli scenari in cui partiti, popolazione iniziale ed eventi sono fissati. Inoltre ogni scenario potrebbe aggiungere dei tracciati speciali (tipo clima che avanza se non si fa niente e influenzi il voto) o delle azioni speciali (se ricchi/poveri è un asse un'azione potrebbe portare dei soldi extra se si è i preferiti di una tessere popolazione ricca in cambio di della corruzione)***   
_Alcune cose sottolineate vanno ancora decise_

## Materiali
* Plancia principale   
  ![La plancia](tabellone.svg)
* Plancia del Giocatore   
  ![Plancia del Giocatore](giocatore.svg)
* Tessere popolazione   
  ![Tessere popolazione](popolazione.svg)
* Cubi colorati per ogni giocatore
* Cubi neutri
* Borsa/Urna elettorale
* Carte Evento  
  ![Carte evento](evento.svg)
* Carte Azione   
  ![Carte azione](azione.svg)

## Scopo del Gioco
Scopo del gioco è di rimanere più anni possibile al governo.

Dopo 5 legislature vince il giocatore che ha passato più anni al governo. Il gioco finisce alternativamente quando un giocatore riesce a completare l'11° anno al governo.

## Rappresentazione della politica
Lo spazio politico viene rappresentato attraverso _4/5_ assi che vanno da _-3/-2/0_ a _+2/+3_ e che potrebbero essere:
* Welfare - Mercato
* Tradizione - Laicità
* Autarchia - Internazionalismo
* Industria - Ambiente

## Preparazione
* Ogni giocatore mette un proprio cubetto sullo 0 del conteggio anni di governo e corruzione.  
* Mettere un segnalino neutro sul primo anno e sulla prima legislatura.
* Ogni giocatore mette 5 propri cubetti colorati nell'urna elettorale.
* Vengono estratte una tessere popolazione per giocatore più due, con due cubi popolazione (neutri) ciascuna.
* Ogni giocatore estrae/sceglie la propria posizione politica. I giocatori scelgono contemporaneamente possibilmente senza vedersi.

## Svolgimento
Il gioco è organizzato in 5 legislature che possono durare al massimo 4 anni.
Ogni legislatura comincia con un'elezione e poi continua con al più 4 turni (anni).

### Elezioni
* I cubetti che attualmente compongono il parlamento vengono rimossi e rimessi nelle riserve dei giocatori.
* Nell'urna vengono inseriti per ogni tessere popolazione N cubetti per ogni partito. Il numero totale di cubetti inseriti per ogni tessera non può superare la popolazione.^[Il numero è pari al numero di cubi popolazione se la posizione del partito è uguale a quella del partito e decrescente al crescere della distanza.] 
* Dall'urna elettorale vengono estratti tanti cubetti quanti i parlamentari da eleggere che verranno posizionati sul tabellone principale a formare il parlamento.
* L'urna elettorale viene svuotata e i cubetti restituiti ai giocatori.
* Ogni giocatore mette 5 cubetti nell'urna (vedi preparazione).

### Anno di legislatura
* Se non c'è un governo in carica va formato un governo (vedi sotto).
* Si pesca una carta evento e si risolve. Se le condizioni della carta evento non sono soddisfatte la carta viene scartata e se ne pesca un'altra.
* A turno ogni giocatore può svolgere un'azione (vedi sotto), seguendo l'ordine (in caso di pareggio in ordine decrescente di numero di parlamentari e successivamente di anni al governo):
    + primo ministro
    + opposizione
    + maggioranza   
* Quando per un intero giro, partendo dal primo ministro, tutti i giocatori passano, l'anno finisce.
* Se il governo dovesse cadere l'anno finisce immediatamente
* Se il governo è in carica alla fine dell'anno, ogni partito in maggioranza avanza di una casella nel conteggio degli anni al governo.
* Ogni partito che è stato al governo nel corso dell'anno avanza di uno sul conteggio Corruzione, ogni partito che è stato all'opposizione diminuisce di uno.

### Formare un governo
* A partire dal giocatore che ha ricevuto più voti (in caso di parità chi ha passato meno anni al governo) si riceve l'incarico di formare il governo.
* Il giocatore con l'incarico può, dopo aver discusso con gli altri giocatori, proporre una posizione del governo spostando gli indicatori sulla plancia principale. Tutti i giocatori dichiarano, in ordine discendente di voti, se sostengono o meno il governo mettendo il proprio cubetto nell'area "Maggioranza" o "Opposizione".
* Se il tentativo fallisce si passa al giocatore successivo.
* Se tutti i giocatori falliscono si va a elezioni e la legislatura termina: il segnalino legislatura avanza di uno, quello dell'anno torna su 1°.
* Se l'accordo riesce i partiti di maggioranza sposteranno la propria posizione politica su _ogni/un_ asse di uno verso la posizione del governo
* Il partito che ha proposto l'accordo è considerato il partito del primo ministro e mette un proprio cubetto nello spazio apposito

### Azioni
Ogni azione ha un costo, dei requisiti che il giocatore deve soddisfare come posizione politica, un effetto dell'azione e un effetto della legge.

|                 Costo | Nome                        | Effetto                                                                                                                                                  |
| --------------------: | :-------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------- |
|                     0 | Tesseramento                | Il partito riceve 1€ per ogni cubetto che inserirebbe dalle tessere popolazione (vedi elezioni) (1 volta per turno)                                      |
|                     0 | Contributi dei parlamentari | Il partito riceve 1€ per ogni parlamentare eletto (1 volta per turno)                                                                                    |
|               1/carta | ?                           | Pesca N carte pagando N€.                                                                                                                                |
|            vedi carta | ?                           | Gioca una carta pagando i costi indicati sulla carta.                                                                                                    |
|                     0 | Cambio ai vertici           | Scarta N carte per ricevere N€                                                                                                                           |
|                     ? | Campagna mediatica          | Aggiungi N cubi popolazione suddivisi a piacere dove N = media controllati + 1 se al governo + 1 se primo ministro +1 se maggiore partito di opposizione |
| ? - media controllati | Riposizionamento            | Sposta la propria posizione su uno degli assi di 1                                                                                                       |
|                     ? | Mobilitare gli iscritti     | Aggiungi un cubo popolazione in una delle tessere popolazione di cui sei rappresentante                                                                  |
|                     0 | Proporre una legge          | Vedi sotto                                                                                                                                               |
|                     ? | ?                           | Aggiungere una tessera popolazione casuale                                                                                                               |

#### Proporre una legge
Ogni carta azione contiene un effetto della legge.   
Per i partiti al governo anche il governo deve soddisfare i requisiti per presentare una legge.   
Finché non si vota un giocatore può ritirare una legge e passare il turno invece che metterla ai voti.   
Quando una legge è presentata tutti i giocatori votano a voto segreto.   
Se la legge è approvata si applica l'effetto.   
Se la legge non è approvata ed è stata proposta da un partito di maggioranza il governo cade e il turno (anno) termina immediatamente.   
Il giocatore che controlla il primo ministro **deve** mettere ai voti almeno una legge ogni anno.

### I media

### Tessere popolazione
_Le tessere popolazione potrebbero essere bianche e il profilo politico si estrae con dei dadi quando vengono pescate OPPURE essere prestampate_   
Si è il rappresentante di una tessera popolazione (mettendo un proprio cubetto nell'apposito spazio solo come promemoria) se si è il partito con la posizione più vicina. In caso di parità vince chi è al governo e successivamente chi ha più parlamentari.   
Il rappresentante si aggiorna ogni volta che cambia uno dei fattori che lo determinano.
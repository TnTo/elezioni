SOURCE = $(wildcard *.tex)
TARGET = $(SOURCE:.tex=.pdf)

.PHONY: all clean

all: $(TARGET)

clean:
	- rm $(TARGET)

regolamento.pdf : regolamento.tex tabellone.pdf
	lualatex --halt-on-error $<
	- rm regolamento.aux regolamento.log regolamento.out

regolamento.tex : elections.sty
	touch $@

%.pdf : %.tex
	lualatex --halt-on-error $<
	- rm $*.aux $*.log 

%.tex : elections.cls elections.sty
	touch $@